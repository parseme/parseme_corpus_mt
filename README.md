README
------
This is the README file from the PARSEME verbal multiword expressions (VMWEs) corpus for Maltese, edition 1.3. See the wiki pages of the [PARSEME corpora initiative](https://gitlab.com/parseme/corpora/-/wikis/home) for the full documentation of the annotation principles.

The present Maltese data result from an update and of the Maltese part of the [PARSEME corpus v 1.0](http://hdl.handle.net/11372/LRT-2282?show=full).
For the changes with respect to version 1.0, see the change log below.

Source corpora
-------
The source data come from Korpus Malti version 3 (news section). More info can be found [here](http://mlrs.research.um.edu.mt/).


Format
-----------------
The data are in the [.cupt](http://multiword.sourceforge.net/cupt-format) format. Here is detailed information about some columns:
* column 3 (LEMMA): Automatically annotated (recopied from version 1.0); no full coverage; where a lemma is not found, the word form is reproduced.
<!-- Lemmas/semitic roots (CoNLL-U) - available (automatically annotated; no full coverage; where a lemma is not found, the word form is reproduced).-->
* column 4 (UPOS): Automatically generated (UDPipe 2, model maltese-mudt-ud-2.10-220711). The tagset is the one of [UD POS-tags](http://universaldependencies.org/u/pos).
* column 5 (XPOS): Automatically annotated. The tagset is [MLRS](http://mlrs.research.um.edu.mt/resources/malti03/tagset30.html).
* column 6 (FEATS): Not available
* column 7 (HEAD): Automatically generated (UDPipe 2, model maltese-mudt-ud-2.10-220711). 
* column 8 (DEPREL): Automatically generated (UDPipe 2, model maltese-mudt-ud-2.10-220711). The tagset is [UD dependency relations](http://universaldependencies.org/u/dep).
* column 10 (MISC): No-space information - available (automatically annotated).
* column 11 (PARSEME:MWE): Manually annotated. The following [categories](http://parsemefr.lif.univ-mrs.fr/parseme-st-guidelines/1.2/?page=030_Categories_of_VMWEs) are used: LVC.full, VID.

Tokenization
------------
* Definite articles and prepositions: The definite article, which is a clitic, is tokenised into a separate token. This is usually separated by a hyphen from its host word (il- ħin is tokenised as "il-" and "ħin"). This is also true of prepositions, when they are definite (mill-ħin = "mill-" + "ħin"). Note, however, that we represent the preposition-article as one token, and the host word as another  (so even though "mill-" = "minn + il-", these are not separated).

Statistics
-------
To know the number of annotated VMWEs of different types and with different properties (length, continuity, etc.), use these scripts: [mwe-stats.py](https://gitlab.com/parseme/utilities/-/blob/master/st-organizers/corpus-statistics/mwe-stats.py) and [mwe-stats-simple.py](https://gitlab.com/parseme/utilities/-/blob/master/st-organizers/corpus-statistics/mwe-stats-simple.py). 


Authors
-------
The VMWEs annotations were performed by (in alphabetical order) Greta Attard, Kirsty Azzopardi, Janice Bonnici, Jael Busuttil, Alison Farrugia, Luke Galea, Sara Anne Galea, Anabelle Gatt, Amanda Muscat, Nicole Tabone, and Marc Tanti.


License
-------
The data are distributed under the terms of the Creative Commons license [CC-BY v4](https://creativecommons.org/licenses/by/4.0/).

Contact
-------
  * lonneke.vanderplas@um.edu.mt, 
  * a.gatt@uu.nl

Future work
-------
* The LEMMA column is empty (the Maltese UD corpus on which UDPipe is train does not contain lemmas). But version 1.0 of the Maltese PARSEME corpus did contain lemma annotations (where a lemma was not found, the word form was reproduced). These could be restored in the current version of the corpus. 
The corpus does not currently have annotations for morphological features (FEATS), and syntax (HEAD and DEPREL). These could be completed whenever a Maltese corpus/model providing these data is available.
* This sentence `il-bank Unicredit wera x-xettic ̧ iy ̈ mu tiegħu kemm verament huwa reali` should probably be corrected to `il-bank Unicredit wera x-xetticiżmu tiegħu kemm verament huwa reali`. 

Change log
----------
- **2023-04-15**:
  - Version 1.3 of the corpus was released on LINDAT.
  - To upgrade the corpus from version 1.0 to 1.3
    - The 133 expressions marked with TODO were manually checked by Albert Gatt and renamed either to NotMWE or to any of the existing VMWE categories
    - The UD-compatible parts-of-speech (in the UPOS) column were generated with UDPipe (maltese-mudt-ud-2.10-220711 model) by Agata Savary
  - For the 1.0 to 1.2 conversion to be complete, the following manual updates are necessary:
    - Find all expressions which pass the LVC.cause tests
    - See also [what is new in the annotation guidelines version 1.1 with respect to 1.0](https://docs.google.com/document/d/15XPEYCK7tE9pTO1yjaqi_oQCtFX_HRszMxCNGjwIDFI/edit?usp=sharing) and in [version 1.2 with respect to 1.1](https://docs.google.com/document/d/1ukhPgfai6kmP5ugTjGbfhE0Wo3GqkYSAtYnnZG6bDgw/edit#heading=h.r4tu3hytzywq)
- **2022-03**:
  - The corpus in version 1.0 was automatically converted from the [.parseme-tsv](https://typo.uni-konstanz.de/parseme/index.php/2-general/184-parseme-shared-task-format-of-the-final-annotation) format (with aligned syntactic annotations in [.conllu](https://universaldependencies.org/format.html)) to the [.cupt](https://multiword.sourceforge.net/cupt-format) format.
  - 133 VMWEs tagged with the OTH category were renamed to TODO. 
  - For the 1.0 to 1.2 conversion to be complete, the following manual updates are necessary:
    - Check the expressions marked with TODO and rename them to one of the existing categories (if appropriate)
    - Find all expressions which pass the LVC.cause tests
    - See also [what is new in the annotation guidelines version 1.1 with respect to 1.0](https://docs.google.com/document/d/15XPEYCK7tE9pTO1yjaqi_oQCtFX_HRszMxCNGjwIDFI/edit?usp=sharing) and in [version 1.2 with respect to 1.1](https://docs.google.com/document/d/1ukhPgfai6kmP5ugTjGbfhE0Wo3GqkYSAtYnnZG6bDgw/edit#heading=h.r4tu3hytzywq)
  - Contact for this update: Carlos Ramisch
- **2017-01-20**:
  - [Version 1.0](http://hdl.handle.net/11372/LRT-2282) of the corpus was released on LINDAT. 
